<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $table = 'bodies';

    protected $fillable = [
        'article_id',
        'type',
        'content',
        'size_align',
    ];

    protected $appends = [
        'content_uri',
    ];

    public static function getTypes()
    {
        return ['text', 'image'];
    }

    public static function getAlignSizes()
    {
        return [
            '100l', // 100% left
            '50l', // 50% left
            '100r', // 100% right
            '50r', // 50% right
            '100c', // 100% center
            '100c', // 100% center
        ];
    }

    public function article()
    {
        return $this->belongsTo(Article::class, 'id', 'article_id');
    }

    public function getContentUriAttribute()
    {
        if ($this->type === 'image') {
            return url('/storage/' . $this->content);
        }
    }
}
