<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';

    protected $fillable = [
        'user_id',
        'title',
        'posted_at',
        'saved_at',
        'slug',
    ];

    protected $dates = [
        'posted_at',
        'saved_at',
    ];

    protected $appends = [
        'cover',
        'intro',
        'link',
        'tag_string',
    ];

    protected $dispatchesEvents = [
        'saved' => \App\Events\MakeArticleSlug::class,
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function body()
    {
        return $this->hasMany(Body::class, 'article_id', 'id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function getCoverAttribute()
    {
        return $this->getFirstBodyContentOfType('image');
    }

    public function getIntroAttribute()
    {
        return $this->getFirstBodyContentOfType('text');
    }

    protected function getFirstBodyContentOfType($type)
    {
        if ($this->body !== null) {
            foreach ($this->body as $body) {
                if ($body->type === $type) {
                    return $body->content_uri;
                }
            }
        }
    }

    protected function getTagStringAttribute()
    {
        if ($this->tags !== null) {
            $tags = '';
            foreach ($this->tags as $tag) {
                $tags .= "$tag->name,";
            }
            return rtrim($tags, ',');
        }
    }

    public function getLinkAttribute()
    {
        if ($this->slug) {
            return url('/p/' . $this->slug);
        }
    }
}
