<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client as GuzzClient;
use Illuminate\Auth\AuthenticationException;

class oAuthVerifyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bearer_token = $request->header('Authorization', null);
        if (is_null($bearer_token)) {
            throw new AuthenticationException("Authorization token not present");
        }

        $this->getUser($request);

        return $next($request);
    }

    protected function getUser($request)
    {
        $client = new GuzzClient([
            'headers' => [
                'Authorization' => $request->header('Authorization'),
            ],
        ]);

        try {
            return $client->get('http://medium.test/api/user');
        } catch (\Exception $e) {
            throw new AuthenticationException("Error Processing Verification Request");
        }
    }
}
