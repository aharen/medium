<?php

namespace App\Http\Requests;

use App\Models\Article;

class TagArticleRequest extends FormRequest
{
    public function authorize()
    {
        if ($this->user() !== null) {
            $article = Article::where('id', $this->route('article'))->firstOrFail();
            return $article->user_id === $this->user()->id;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => 'required|array',
        ];
    }
}
