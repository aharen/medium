<?php

namespace App\Http\Requests;

use App\Models\Article;

class UpdateArticleRequest extends FormRequest
{
    public function authorize()
    {
        if ($this->user() !== null) {
            $article = Article::where('id', $this->route('article'))->firstOrFail();
            return $article->user_id === $this->user()->id;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255',
            'published' => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }
}
