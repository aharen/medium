<?php

namespace App\Http\Requests;

class StoreTagRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user() === null ? false : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:tags|max:255',
        ];
    }
}
