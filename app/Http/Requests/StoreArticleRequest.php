<?php

namespace App\Http\Requests;

class StoreArticleRequest extends FormRequest
{
    public function authorize()
    {
        return $this->user() === null ? false : true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     => 'required|max:255',
            'published' => 'nullable|date_format:Y-m-d H:i:s',
        ];
    }
}
