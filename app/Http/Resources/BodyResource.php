<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BodyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type'    => $this->type,
            'content' => $this->type === 'image' ? $this->content_uri : $this->content,
            'size'    => $this->size_align,
        ];
    }
}
