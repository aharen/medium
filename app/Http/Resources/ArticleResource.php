<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'         => $this->id,
            'title'      => $this->title,
            'cover'      => $this->cover,
            'intro'      => $this->intro,
            'body'       => BodyResource::collection($this->body),
            'tags'       => TagResource::collection($this->tags),
            'tag_string' => $this->tag_string,
            'published'  => $this->posted_at,
            'user'       => new UserResource($this->user),
            'link'       => $this->link,
            'slug'       => $this->slug,
        ];
    }
}
