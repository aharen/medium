<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource as LaravelJsonResource;

class Resource extends LaravelJsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function width($request)
    {
        return [
            'code'       => '200',
            'message'    => 'OK',
            'pagination' => null,
        ];
    }
}
