<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SearchTagRequest;
use App\Http\Requests\StoreTagRequest;
use App\Http\Resources\TagResource;
use App\Models\Tag;

class TagController extends ApiController
{

    protected $tag_fields = ['id', 'name', 'slug'];

    public function index()
    {
        $tags = Tag::select($this->tag_fields)->get();

        return TagResource::collection(
            $tags
        );
    }

    public function search(SearchTagRequest $request)
    {
        $keyword = $request->keyword;
        $results = Tag::select($this->tag_fields)
            ->where('name', 'LIKE', "%$keyword%")
            ->get();

        return TagResource::collection(
            $results
        );
    }

    public function store(StoreTagRequest $request)
    {
        $tag = new Tag([
            'name' => $request->name,
            'slug' => str_slug($request->name),
        ]);
        $tag->save();

        return new TagResource($tag);
    }
}
