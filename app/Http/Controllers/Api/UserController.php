<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

class UserController extends ApiController
{
    public function index(Request $request)
    {
        return $request->user();
    }
}
