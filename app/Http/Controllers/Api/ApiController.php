<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Image;

class ApiController extends Controller
{
    protected $limit = 12;

    protected function respond($data, $message = 'OK', $code = 200)
    {
        return response()->json([
            // 'message' => $message,
            'data' => $data,
        ], $code);
    }

    protected function saveImage($save_dir, $data, int $width, $thumb = false)
    {
        $save_dir     = str_slug($save_dir);
        $relative_dir = implode('/', [
            $save_dir,
            date('Y'),
            date('m'),
        ]);

        $save_path = implode('/', [
            config('medium.image.path'),
            $relative_dir,
        ]);

        if (!is_dir($save_path)) {
            mkdir($save_path, 0755, true);
        }

        $filename  = base64_encode(md5(time() . uniqid()));
        $base_name = $filename . '.jpg';

        $image = Image::make($data);

        if ($image->width() <= $width) {
            $image->save($save_path . '/' . $base_name, 80);
        } else {
            $image->resize($width, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($save_path . '/' . $base_name, 80);
        }

        if ($thumb === true) {
            $base_name_thumb = $filename . '_thumb.jpg';
            $image->crop(500, 500)->save($save_path . '/' . $base_name_thumb, 80);
        }

        return $relative_dir . '/' . $base_name;
        // return $base_name;
    }

    protected function deleteImage($file)
    {
        $fullPath = implode('/', [
            config('medium.image.path'),
            $file,
        ]);

        if (is_file($fullPath)) {
            unlink($fullPath);
        }
    }

    protected function isDatauri($string)
    {
        $regex = '/^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i';
        return preg_match($regex, $string);
    }
}
