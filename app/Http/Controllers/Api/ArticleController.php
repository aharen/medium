<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\StoreArticleRequest;
use App\Http\Requests\TagArticleRequest;
use App\Http\Requests\UpdateArticleRequest;
use App\Http\Resources\ArticleResource;
use App\Models\Article;
use App\Models\Body;
use App\Models\Tag;
use App\User;
use Illuminate\Http\Request;

class ArticleController extends ApiController
{
    protected $article_fields = [
        'id', 'user_id', 'title', 'slug', 'posted_at', 'saved_at', 'created_at', 'updated_at',
    ];

    protected $user_fields = [
        'id', 'name', 'username', 'email', 'email_verified_at', 'created_at',
    ];

    protected $body_fields = [
        'id', 'article_id', 'type', 'content', 'size_align',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('posted_at', '!=', null)->orderBy('id', 'desc')->paginate($this->limit, $this->article_fields);

        return ArticleResource::collection(
            $articles
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreArticleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreArticleRequest $request)
    {
        $article = new Article([
            'user_id'   => $request->user()->id,
            'title'     => $request->title,
            'posted_at' => ($request->has('published')) ? $request->published : null,
        ]);
        $article->save();

        foreach ($request->body as $body) {
            if ($body['type'] === 'image') {
                if ($this->isDatauri($body['content'])) {
                    $body['content'] = $this->saveImage('posts', $body['content'], 1024);
                }
            }

            $article->body()->save(new Body([
                'type'       => $body['type'],
                'content'    => $body['content'],
                'size_align' => isset($body['size']) ? $body['size'] : '100l',
            ]));
        }

        if ($request->tags !== null) {
            $tag_ids = [];
            foreach (array_values(explode(',', $request->tags)) as $tag) {
                $tag = Tag::firstOrCreate([
                    'name' => $tag,
                    'slug' => str_slug($tag),
                ]);
                $tag->save();
                $tag_ids[] = $tag->id;
            }
            $article->tags()->sync($tag_ids);
        }

        return new ArticleResource(
            $article
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $where_field = ($request->has('link')) ? 'slug' : 'id';

        $article = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where($where_field, $id)->firstOrFail();

        return new ArticleResource(
            $article
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateArticleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateArticleRequest $request, $id)
    {
        $article = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('id', $id)->firstOrFail();

        $article->title     = $request->title;
        $article->posted_at = ($request->has('published')) ? $request->published : null;
        $article->save();

        $article->body()->delete();

        foreach ($request->body as $body) {
            if ($body['type'] === 'image') {
                if ($this->isDatauri($body['content'])) {
                    $body['content'] = $this->saveImage('posts', $body['content'], 1024);
                } else {
                    $ex              = explode('posts', $body['content']);
                    $body['content'] = 'posts/' . $ex[1];
                }
            }

            $article->body()->save(new Body([
                'type'       => $body['type'],
                'content'    => $body['content'],
                'size_align' => isset($body['size']) ? $body['size'] : '100l',
            ]));
        }

        if ($request->tags !== null) {
            $tag_ids = [];
            foreach (array_values(explode(',', $request->tags)) as $tag) {
                $tag = Tag::firstOrCreate([
                    'name' => $tag,
                    'slug' => str_slug($tag),
                ]);
                $tag->save();
                $tag_ids[] = $tag->id;
            }
            $article->tags()->sync($tag_ids);
        }

        $article = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('id', $id)->firstOrFail();

        return new ArticleResource(
            $article
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('id', $id)->firstOrFail();

        $article->delete();

        return $this->respond('Article Deleted');
    }

    public function tag($id, TagArticleRequest $request)
    {
        $article = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('id', $id)->firstOrFail();

        $tag_ids = [];
        foreach ($request->tags as $tag) {
            $tag = Tag::firstOrCreate([
                'name' => $tag,
                'slug' => str_slug($tag),
            ]);
            $tag->save();

            $tag_ids[] = $tag->id;
        }

        $article->tags()->sync($tag_ids);

        return new ArticleResource($article);
    }

    public function userArticle($username)
    {
        $user = User::select('id', 'username')
            ->where('username', $username)
            ->firstOrFail();

        $articles = Article::select(
            $this->article_fields
        )->with(['user' => function ($q) {
            return $q->select($this->user_fields);
        }, 'body' => function ($q) {
            return $q->select($this->body_fields);
        }])->where('posted_at', '!=', null)
            ->where('user_id', $user->id)
            ->orderBy('id', 'desc')
            ->paginate($this->limit, $this->article_fields);

        return ArticleResource::collection(
            $articles
        );
    }
}
