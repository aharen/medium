<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use JWTAuth;

class HomeController extends WebController
{

    protected $article_fields = [];

    public function index(Request $request)
    {
        $user  = auth()->user();
        $token = null;
        if ($user !== null) {
            $user['dp'] = $this->getProfilepicture($user['email']);
            $token      = JWTAuth::fromUser($user);
        }

        $api_base_uri = config('medium.api_base_uri');
        return response()->view('layouts.web', compact('token', 'api_base_uri', 'user'));
    }

    protected function getProfilepicture($email, $s = 100, $d = 'mm', $r = 'g', $img = false, $atts = array())
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }

            $url .= ' />';
        }
        return $url;
    }
}
