<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class WebController extends Controller
{
    protected $article_fields = [
        'id', 'user_id', 'title', 'posted_at', 'saved_at', 'created_at', 'updated_at',
    ];

    protected $body_fields = [
        'id', 'article_id', 'type', 'content', 'size_align',
    ];

    protected $tag_fields = ['id', 'name', 'slug'];

    protected $user_fields = [
        'id', 'name', 'username', 'email', 'email_verified_at', 'created_at',
    ];

    protected $token = null;

    public function __construct(Request $request)
    {
        $this->token = $this->checkAndGetAccessToken($request);
    }

    public function respond($data)
    {
    }

    protected function checkAndGetAccessToken($request)
    {
        if ($request->user() !== null) {
            return $user->createToken(config('medium.oauth.client_name'))->accessToken;
        }
        return null;
    }

}
