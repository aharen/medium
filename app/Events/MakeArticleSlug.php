<?php

namespace App\Events;

use App\Models\Article;
use Illuminate\Queue\SerializesModels;

class MakeArticleSlug
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Article $article)
    {
        $slug = str_slug($article->title . ' ' . base64_encode($article->id . $article->created_at));
        if ($article->slug !== $slug) {
            $article->slug = $slug;
            $article->save();
        }
    }
}
