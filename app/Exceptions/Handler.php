<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($this->isApiCall($request) === true) {
            if ($this->hasValidContentType($request) === false) {
                // return $this->respondWithJson(415, 'Unsupported Media Type');
            }

            if ($exception instanceof NotFoundHttpException || $exception instanceof ModelNotFoundException) {
                return $this->respondWithJson(404, 'Not Found');
            }

            if ($exception instanceof AuthenticationException) {
                return $this->respondWithJson(401, 'Unauthenticated');
            }

            if ($exception instanceof AuthorizationException) {
                return $this->respondWithJson(403, 'Unauthorized');
            }

            if ($exception instanceof ValidationException) {
                return $this->respondWithJson(422, 'Validation Error', $exception->errors());
            }

            dd($exception);

            return $this->respondWithJson(
                $exception->getStatusCode(),
                Response::$statusTexts[$exception->getStatusCode()]
            );
        }

        return parent::render($request, $exception);
    }

    protected function isApiCall($request)
    {
        return ($request->is('api/*')) ? true : false;
    }

    protected function hasValidContentType($request)
    {
        return ($request->header('content-type') === 'application/json') ? true : false;
    }

    protected function respondWithJson($code, $message, $data = null)
    {
        return response()->json([
            'code'    => $code,
            'message' => $message,
            'data'    => $data,
        ], $code);
    }
}
