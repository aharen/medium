const mix = require('laravel-mix');

require('laravel-mix-tailwind');
require('laravel-mix-purgecss');

mix.js('resources/js/app.js', 'public/js').extract([
	'vue',
	'vue-router',
	'axios',
	'lodash',
	'popper.js'
]);

mix.postCss('resources/css/app.css', 'public/css');
mix.tailwind();
mix.purgeCss();

mix.copy('resources/svg', 'public/svg');

// if (mix.inProduction()) {
mix.version();
// }