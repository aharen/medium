import VueRouter from 'vue-router'

const Home = (resolve) => require(['./components/Home.vue'], resolve);
const Post = (resolve) => require(['./components/Post.vue'], resolve);
const User = (resolve) => require(['./components/User.vue'], resolve);
const Tag = (resolve) => require(['./components/Tag.vue'], resolve);
const Compose = (resolve) => require(['./components/Compose.vue'], resolve);
const NotFound = (resolve) => require(['./components/NotFound.vue'], resolve);

const router = new VueRouter({
	mode: 'history',
	base: __dirname,
	routes: [{
		path: '/',
		name: 'Home',
		component: Home
	}, {
		path: '/t/:id',
		name: 'Tag',
		component: Tag
	}, {
		path: '/u/:id',
		name: 'User',
		component: User
	}, {
		path: '/p/:id',
		name: 'Post',
		component: Post
	}, {
		path: '/compose',
		name: 'Compose',
		component: Compose
	}, {
		path: '/compose/:id',
		name: 'Edit Compose',
		component: Compose
	}, {
		path: '/404',
		name: 'Not Found',
		component: NotFound
	}, {
		path: '*',
		redirect: '/404'
	}]
})

// router.beforeEach((to, from, next) => {
// 	if (to.fullPath !== '/403' && to.fullPath !== '/404') {} else {
// 		next();
// 	}
// });

export default router;