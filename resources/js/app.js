require('./bootstrap');

import VueRouter from 'vue-router'
import router from './router.js'
import Vue from 'vue';
import App from './App.vue'

// try {
// 	window.Vue = require('vue');

// } catch (e) {
// 	console.error('APP Bootstrap Error');
// 	console.dir(e);
// }


Vue.use(VueRouter);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./components/', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('App', require('./components/App.vue'));
// Vue.component('Home', require('./components/Home.vue'));
// Vue.component('Post', require('./components/Post.vue'));

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

new Vue({
	router,
	extends: App,
	data: {
		user: (window.MEDIUMS && window.MEDIUMS.user) ? window.MEDIUMS.user : null,
		pageLoading: true
	}
}).$mount('#app');