window._ = require('lodash');
window.Popper = require('popper.js').default;

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

// window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

// set api baseUri
let baseURL = document.head.querySelector('meta[name="api-base-uri"]');
window.axios.defaults.baseURL = baseURL.content;

let csrf_token = document.head.querySelector('meta[name="csrf-token"]');
let token = document.head.querySelector('meta[name="token"]');

if (token) {
	window.axios.defaults.headers.common['X-CSRF-TOKEN'] = csrf_token.content;
	window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token.content;
} else {
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

// Add a 401 and 403 response interceptor
window.axios.interceptors.response.use(function(response) {
	return response;
}, function(error) {
	if (401 === error.response.status) {
		window.location = window.location.href;
	}
	return Promise.reject(error);
});