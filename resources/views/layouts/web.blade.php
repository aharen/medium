<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="token" content="{{ $token }}">

    <meta name="api-base-uri" content="{{ $api_base_uri }}">

    <title>{{ config('app.name', 'MEDIUM') }}</title>
	<link rel="stylesheet" href="{{ mix('/css/app.css') }}" />
    <script>
window.MEDIUMS = <?php echo json_encode([
    'user' => $user
]); ?>
    </script>
</head>
<body class="h-screen antialiased">	
	<div id="app"></div>
	<script src="{{ mix('/js/manifest.js') }}"></script>
	<script src="{{ mix('/js/vendor.js') }}" defer></script>
	<script src="{{ mix('/js/app.js') }}" defer></script>
</body>
</html>