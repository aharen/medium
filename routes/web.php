<?php

Auth::routes();

// Route::get('/{any}', 'Web\HomeController@index')->where('any', '.*');

Route::get('/', 'Web\HomeController@index')->name('home');
Route::get('/compose/{id}', 'Web\HomeController@index')->middleware('auth');
Route::get('/compose', 'Web\HomeController@index')->middleware('auth');
Route::get('/p/{link}', 'Web\HomeController@index');
Route::get('/u/{link}', 'Web\HomeController@index');
