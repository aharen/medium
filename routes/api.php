<?php

// Route::group(['domain' => config('medium.api_base_uri')], function () {
Route::post('/access-token', 'Api\Auth\AccessTokensController@store');
Route::post('/refresh-token', 'Api\Auth\AccessTokensController@store');

Route::group(['middleware' => 'api'], function () {

    Route::get('/user', 'Api\UserController@index');

    Route::group(['prefix' => 'article'], function () {
        Route::get('/', 'Api\ArticleController@index')->name('article.index');
        Route::post('/', 'Api\ArticleController@store')->name('article.store')->middleware('jwt:api');
        Route::delete('/{article}', 'Api\ArticleController@destroy')->name('article.destroy')->middleware('jwt:api');
        Route::patch('/{article}', 'Api\ArticleController@update')->name('article.update')->middleware('jwt:api');
        Route::get('/user/{username}', 'Api\ArticleController@userArticle')->name('article.userArticle');
        Route::get('/{article}', 'Api\ArticleController@show')->name('article.show');
        Route::post('/{article}/tag', 'Api\ArticleController@tag')->name('article.tag')->middleware('jwt:api');
    });

    Route::group(['prefix' => 'tag'], function () {
        Route::get('/', 'Api\TagController@index')->name('tag.index');
        Route::post('/', 'Api\TagController@store')->name('tag.store')->middleware('jwt:api');
        Route::post('/search', 'Api\TagController@search')->name('tag.search');
    });
});

Route::fallback(function () {
    return response()->json([
        'code'    => 404,
        'message' => 'Not Found',
        'data'    => null,
    ], 404);
})->name('api.fallback.404');
// });
