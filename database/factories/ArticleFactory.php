<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    return [
        'user_id' => \App\User::inRandomOrder()->whereNotNull('email_verified_at')->first()['id'],
        'title'   => function () use ($faker) {
            return $faker->sentence(rand(5, 20));
        },
    ];
});
