<?php

use App\Models\Body;
use Faker\Generator as Faker;

$factory->define(Body::class, function (Faker $faker) {
    $types = Body::getTypes();
    $type  = $types[rand(0, 1)];

    // $align_sizes = Body::getAlignSizes();
    // $align_size  = $align_sizes[rand(0, 5)];
    $align_size = '100l';

    $image_path = config('medium.image.path');
    // $content    = ($type === 'text') ? $faker->paragraph(4, 10) : $faker->imageUrl();
    $content = ($type === 'text') ? $faker->paragraph(4, 10) : 'posts/' . $faker->image($image_path . '/posts', 400, 300, null, false);

    return [
        'type'       => $type,
        'content'    => $content,
        'size_align' => $align_size,
    ];
});
