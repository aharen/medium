<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Article::class, 60)->create()->each(function ($article) {

            /* publish some articles */
            if (rand(0, 1) === 1) {
                $article->posted_at = Carbon::now();
                $article->save();
            }

            for ($i = 0; $i < rand(5, 20); $i++) {
                $article->body()->save(
                    factory(App\Models\Body::class)->make()
                );
            }
        });
    }
}
