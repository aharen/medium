Hi 👋

## Info

Development was done on laravel homestead running v7.18.0. JWT is used to talked to authenticated end-points.

## Installation

Clone the Repo

`git clone git@bitbucket.org:aharen/medium.git`

Go in  to the folder and run composer install

`cd medium && composer install`

Copy the environment file

`cp .env.example .env`

Generate Application key

`php artisan key:generate`

## Environment Configs

JWT Secret for JWT Tokens

`
JWT_SECRET=AuNQ4SXkBM73xm4DRsRYx9pdVBE5tLyH
`

API Base URI. Used by front-end to make API calls, so must set correctly.

`
API_BASE_URI=http://medium.test/api/
`

Absolute path to your image storage

`
IMAGE_PATH=/home/vagrant/Code/medium/public/storage
`

Database Configs


`
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=medium
DB_USERNAME=homestead
DB_PASSWORD=secret
`
