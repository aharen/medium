<?php

return [
    'api_base_uri' => env('API_BASE_URI'),
    'oauth'        => [
        'client_name'   => env('OAUTH_CLIENT_NAME'),
        'client_id'     => env('OAUTH_CLIENT_ID'),
        'client_secret' => env('OAUTH_CLIENT_SECRET'),
        'grant_type'    => env('OAUTH_GRANT_TYPE'),
    ],
    'image'        => [
        'path' => env('IMAGE_PATH'),
    ],
];
