/*

Tailwind - The Utility-First CSS Framework

A project by Adam Wathan (@adamwathan), Jonathan Reinink (@reinink),
David Hemphill (@davidhemphill) and Steve Schoger (@steveschoger).

Welcome to the Tailwind config file. This is where you can customize
Tailwind specifically for your project. Don't be intimidated by the
length of this file. It's really just a big JavaScript object and
we've done our very best to explain each section.

View the full documentation at https://tailwindcss.com.
*/

let defaultConfig = require('tailwindcss/defaultConfig')()

module.exports = {
	defaultConfig,
	plugins: [
		require('tailwindcss/plugins/container')({
			padding: '10px'
		})
	]
}